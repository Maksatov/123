﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Interfaces;
using WebApplication3.Models;
using WebApplication3.ViewModels;

namespace WebApplication3.Controllers
{
    public class CarsController : Controller
    {
        private readonly ApplicationDbContent _dbContext;
        private readonly IAllCar _allCar;
        private readonly ICarsCategory _carsCategory;

        public CarsController(IAllCar iAllCar, ICarsCategory iCarsCat, ApplicationDbContent dbContent)
        {
            _dbContext = dbContent;
            _allCar = iAllCar;
            _carsCategory = iCarsCat;
        }



        public ViewResult List()
        {
            CarsListViewModel obj = new CarsListViewModel();
            obj.allCars = _allCar.Cars;
            obj.currCategory = "Cars";

            
        
            return View(obj);
        }
    }
}