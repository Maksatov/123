﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContent _dbContext;

        public HomeController(ApplicationDbContent dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            Category category = new Category();
            category.categoryName = "sdf";
            category.categoryDesc = "sdf";

            _dbContext.Category.Add(category);

            Car car = new Car();
            car.Category = category;
            car.name = "a";
            _dbContext.Car.Add(car);

            _dbContext.SaveChanges();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
