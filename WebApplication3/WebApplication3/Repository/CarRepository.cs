﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Interfaces;
using WebApplication3.Models;

namespace WebApplication3.Repository
{
    public class CarRepository : IRepository<Car>
    {
        private ApplicationDbContent db;

        public CarRepository(ApplicationDbContent context)
        {
            this.db = context;
        }

        public IEnumerable<Car> GetAll()
        {
            return db.Car;
        }

        public Car Get(int id)
        {
            return db.Car.Find(id);
        }

        public void Create(Car car)
        {
            db.Car.Add(car);
        }

        public void Update(Car car)
        {
            db.Entry(car).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Car car = db.Car.Find(id);
            if (car != null)
                db.Car.Remove(car);
        }
    }
}
