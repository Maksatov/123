﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Interfaces;
using WebApplication3.Models;

namespace WebApplication3.Repository
{
    public class CategoryRepository : IRepository<Category>
    {
        private ApplicationDbContent db;

        public CategoryRepository(ApplicationDbContent context)
        {
            this.db = context;
        }

        public IEnumerable<Category> GetAll()
        {
            return db.Category.Include(o => o.cars);
        }

        public Category Get(int id)
        {
            return db.Category.Find(id);
        }

        public void Create(Category category)
        {
            db.Category.Add(category);
        }

        public void Update(Category category)
        {
            db.Entry(category).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Category category = db.Category.Find(id);
            if (category != null)
                db.Category.Remove(category);
        }
    }
}
