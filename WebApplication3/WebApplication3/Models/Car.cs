﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class Car
    {
        public int id { set; get; }
        public string name { set; get; }
        public string desc { set; get; }
        public ushort price { set; get; }
        public int categoryID { set; get; }
        public virtual Category Category { set; get; }
    }
}
