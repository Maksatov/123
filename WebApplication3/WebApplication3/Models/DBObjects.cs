﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class DBObjects
    {
        public static void Initial(ApplicationDbContent content)
        {
            
            

            if (!content.Category.Any())
                content.Category.AddRange(Categories.Select(c=>c.Value));

            if (!content.Category.Any())
            {
                content.AddRange(
                    //new Car { name = "Tesla", desc = "", price = 65000, Category = Categories["Electro"] },
                    //new Car { name = "Ford Fiesta", desc = "", price = 17000, Category = Categories["Classic"] },
                    //new Car { name = "BMW M3", desc = "", price = 65500, Category = Categories["Classic"] },
                    //new Car { name = "Mercedec c class", desc = "", price = 45000, Category = Categories["Classic"] }
               );
            }
            content.SaveChanges();
        }
        private static Dictionary<string, Category> category;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (category == null)
                {
                    var list = new Category[]
                    {
                        new Category{categoryName="Electro",categoryDesc="Newest"},
                        new Category{categoryName="Class",categoryDesc="Old"}
                    };
                    category = new Dictionary<string, Category>();
                    foreach (Category el in list)
                        category.Add(el.categoryName, el);
                }
                return category;
            }
        }
    }
}
