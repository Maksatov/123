﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class ApplicationDbContent:DbContext
    {
        public ApplicationDbContent()
        {
        }

        public ApplicationDbContent(DbContextOptions<ApplicationDbContent> options) : base(options)
        {

        }
        public DbSet<Car> Car { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}
